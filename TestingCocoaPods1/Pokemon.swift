//
//  Pokemon.swift
//  TestingCocoaPods1
//
//  Created by Cristobal Navarrete on 12/3/17.
//  Copyright © 2017 Cristobal Navarrete. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon:Mappable {
    
    var pkName:String?
    var pkWeight:String?
    var pkHeight:String?
    var pkBorn:String?
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        pkName <- map["name"]
        pkWeight <- map["mass"]
        pkHeight <- map["height"]
        pkBorn <- map["birth_year"]
        
    }
}
