//
//  ViewController.swift
//  TestingCocoaPods1
//
//  Created by Cristobal Navarrete on 12/2/17.
//  Copyright © 2017 Cristobal Navarrete. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var bornLabel: UILabel!
    //@IBOutlet weak var capturaTexto: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func consultarButton(_ sender: Any) {
        let pkId = arc4random_uniform(19) + 1
        Alamofire.request("https://swapi.co/api/people/\(pkId)").responseObject { (response: DataResponse<Pokemon>) in

                let pokemon = response.result.value
                DispatchQueue.main.async {
                    self.nameLabel.text = pokemon?.pkName
                    self.weightLabel.text = pokemon?.pkWeight
                    self.heightLabel.text = pokemon?.pkHeight
                    self.bornLabel.text = pokemon?.pkBorn
                }
    
        
        }
        
        
    }
    
}

